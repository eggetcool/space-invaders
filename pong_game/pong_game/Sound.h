#pragma once

class Sound
{
public:
	Sound(Mix_Chunk* p_pxSoundClip);
	~Sound();
	void PlaySound();

private:
	Mix_Chunk* m_pxSoundClip;
};