#include "stdafx.h"
#include "GameState.h"
#include "SpriteManager.h"
#include "DrawManager.h"
#include "Keyboard.h"
#include "Sprite.h"
#include "PlayerShip.h"
#include "Projectile.h"
#include "AlienProj.h"
#include "Alien.h"
#include "AlienRect.h"
#include "CollisionManager.h"
#include "EndState.h"
#include "Cover.h"
#include "SDL_ttf.h"
#include "LifeManager.h"
#include "MenuState.h"
#include "WinState.h"
#include "MysteryShip.h"
#include "MysteryShipProjectile.h"
#include "InputManager.h"
#include "Sound.h"
#include "SoundManager.h"

GameState::GameState(System& p_xSystem)
{
	m_xSystem = p_xSystem;
	m_pxPlayerShip = nullptr;
	m_pxProjectile = nullptr;
	m_pxMysteryProj = nullptr;
	m_pxAlienRect = nullptr;
	m_pxStateManager = nullptr;
	m_pxAlienProj = nullptr;
	m_pxCover = nullptr;
	m_pxMusic = nullptr;
	m_pxShoot = nullptr;
	m_pxLifeManager = nullptr;
	m_pxMysteryShip = nullptr;
	//gFont = nullptr;
	textColor = { 255, 255, 255 };
	life = 0;
	invadersdead = 0;
	score = 0;
	Mix_Init(MIX_INIT_MP3);
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
	}

	/*m_pxShoot = Mix_LoadWAV("../assets/plop.wav");
	if (m_pxShoot == nullptr)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
	}*/

	/*if (TTF_Init() == -1)
	{
		const char* error = TTF_GetError();
		SDL_Log(error);
	}*/
}


GameState::~GameState()
{
	/*Mix_FreeChunk(m_pxShoot);
	m_pxShoot = nullptr;*/
}

void GameState::Enter()
{
	m_pxShoot = m_xSystem.m_pxSoundManager->CreateSound("../assets/plop.wav");
	////////////////////
	Sprite* xSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/main.bmp", 0, 0, 21, 8);
	SDL_Rect* xRect = xSprite->GetRegion();
	int iHeight = xRect->h;

	//Playership
	m_pxPlayerShip = new PlayerShip(m_xSystem.m_pxInputManager, xSprite, m_xSystem.m_iScreenWidth / 2, 
		m_xSystem.m_iScreenHeight - 20 - iHeight, m_xSystem.m_iScreenWidth);

	//Life Sprite
	for (int i = 0; i < 3; i++)
	{
		m_pxLifeManager = new LifeManager(xSprite, 30 * i, 10);
		m_apxLife.push_back(m_pxLifeManager);
	}
	//Player projectile
	m_pxProjectile = new Projectile(m_xSystem.m_pxInputManager,
		m_xSystem.m_pxSpriteManager->CreateSprite("../assets/main.bmp", 0, 10, 1, 4),
		m_xSystem.m_iScreenWidth,
		m_xSystem.m_iScreenHeight);

	
	//Invaders
	SDL_Rect blockCoords[] = 
	{
		{23, 0, 21, 8},
		{46, 0, 21, 8},
		{69, 0, 21, 8}
	};
	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			SDL_Rect& rect = blockCoords[j % 3];
			Alien* pxAlien = new Alien(
				m_xSystem.m_pxSpriteManager->CreateSprite("../assets/main.bmp", rect.x, rect.y, rect.w, rect.h),
				10 + i * 50.0f,
				40 + j * 20);
			m_apxAlien.push_back(pxAlien);
		}
	}

	//Alien rektangel hitbox.
	ResizeRect();
	
	//Alien skott
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			m_pxAlienProj = new AlienProj(m_xSystem.m_pxSpriteManager->CreateSprite("../assets/main.bmp",
				3, 9, 2, 6), 
				10 + i * 50.0f,
				10 + j *20);
			m_apxAlienP.push_back(m_pxAlienProj);
		}
	}

	//Skydd
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			for (int x = 0; x < 7; x++)
			{
				m_pxCover = new Cover(m_xSystem.m_pxSpriteManager->CreateSprite("../assets/main.bmp",
					0, 27, 5, 9),
					100 + (i*125.0f) + x*6,
                    400 + j * 9.0f);
				m_apxCover.push_back(m_pxCover);
			}
			
		}
	}
	//MysteryShip
	m_pxMysteryShip = new MysteryShip(m_xSystem.m_pxSpriteManager->CreateSprite("../assets/main.bmp",
		69, 8, 21, 8), 0, 20);

	m_pxMysteryProj = new MysteryShipProjectile(m_xSystem.m_pxSpriteManager->CreateSprite("../assets/main.bmp",
		69, 8, 2, 4),
		m_xSystem.m_iScreenWidth,
		m_xSystem.m_iScreenHeight);
	//stupid Font
	//gFont = TTF_OpenFont("../assets/AGENCYR.TTF", 28);
}

void GameState::Exit()
{
	//Delete MysteryShipProjectile
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxMysteryProj->GetSprite());
	delete m_pxMysteryProj;
	m_pxMysteryProj = nullptr;

	//Delete MysteryShip
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxMysteryShip->GetSprite());
	delete m_pxMysteryShip;
	m_pxMysteryShip = nullptr;

	//Delete Covers
	auto covit = m_apxCover.begin();
	while (covit != m_apxCover.end())
	{
		m_xSystem.m_pxSpriteManager->DestroySprite((*covit)->GetSprite());
		delete (*covit);
		covit++;
	}

	//Delete Alien Projectiles
	auto projit = m_apxAlienP.begin();
	while (projit != m_apxAlienP.end())
	{
		m_xSystem.m_pxSpriteManager->DestroySprite((*projit)->GetSprite());
		delete *projit;
		projit++;
	}
	m_apxAlienP.clear();

	//Delete Alien Rectangle
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxAlienRect->GetSprite());
	delete m_pxAlienRect;
	m_pxAlienRect = nullptr;

	//Delete Aliens
	auto it = m_apxAlien.begin();
	while (it != m_apxAlien.end())
	{
		m_xSystem.m_pxSpriteManager->DestroySprite((*it)->GetSprite());
		delete *it;
		it++;
	}
	m_apxAlien.clear();

	//Delete Player Projectile
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxProjectile->GetSprite());
	delete m_pxProjectile;
	m_pxProjectile = nullptr;

	//Delete LifeManager
	auto lifit = m_apxLife.begin();
	while (lifit != m_apxLife.end())
	{
		m_xSystem.m_pxSpriteManager->DestroySprite((*lifit)->GetSprite());
		delete *lifit;
		lifit++;
	}
	m_apxLife.clear();

	//Delete Player Ship
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxPlayerShip->GetSprite());
	delete m_pxPlayerShip;
	m_pxPlayerShip = nullptr;

	//Delete StateManager
	delete m_pxStateManager;
	m_pxStateManager = nullptr;
}

bool GameState::Update(float p_fDeltaTime)
{
	//message = TTF_RenderText_Solid(gFont, "The quick brown fox jumps over the lazy dog", textColor);
	m_pxPlayerShip->Update(p_fDeltaTime);
	m_pxProjectile->Update(p_fDeltaTime);
	m_pxAlienRect->Update(p_fDeltaTime);
	m_pxMysteryShip->Update(p_fDeltaTime);
	if (m_pxMysteryShip->IsVisible())
	{
		m_pxMysteryProj->Update(p_fDeltaTime);
		m_xSystem.m_pxDrawManager->Draw(m_pxMysteryProj->GetSprite(), m_pxMysteryProj->GetX(), m_pxMysteryProj->GetY());
	}
	//Player Projectile
	if (m_pxProjectile->IsActive() == false)
	{
		m_pxProjectile->SetPosition(m_pxPlayerShip->GetX() + m_pxPlayerShip->GetSprite()->GetRegion()->w / 2 - m_pxProjectile->GetSprite()->GetRegion()->w / 2,
			m_pxPlayerShip->GetY() + m_pxPlayerShip->GetSprite()->GetRegion()->h / 2 - m_pxProjectile->GetSprite()->GetRegion()->h / 2);
	}
	if (m_pxProjectile->GetY() < 0)
	{
		m_pxProjectile->Deactivate();
		m_pxProjectile->SetPosition(m_pxPlayerShip->GetX() + m_pxPlayerShip->GetSprite()->GetRegion()->w / 2,
			m_pxPlayerShip->GetY());
	}
	//Mystery Projectile
	if (m_pxMysteryProj->IsActive() == false)
	{
		m_pxMysteryProj->SetPosition(m_pxMysteryShip->GetX() + m_pxMysteryShip->GetSprite()->GetRegion()->w / 2 - m_pxMysteryShip->GetSprite()->GetRegion()->w / 2,
			m_pxMysteryShip->GetY() + m_pxMysteryShip->GetSprite()->GetRegion()->h / 2 - m_pxMysteryShip->GetSprite()->GetRegion()->h / 2);
	}
	if (m_pxMysteryProj->GetY() > m_xSystem.m_iScreenHeight)
	{
		m_pxMysteryProj->Deactivate();
		m_pxMysteryProj->SetPosition(m_pxMysteryShip->GetX() + m_pxMysteryShip->GetSprite()->GetRegion()->w / 2,
			m_pxMysteryShip->GetY());
	}

	//MysteryShip Fire
	/*if (m_pxMysteryShip->IsVisible() && m_pxMysteryProj->IsVisible())
	{
		if (m_pxMysteryProj->IsActive() == false)
		{
			m_pxMysteryProj->SetPosition(m_pxMysteryShip->GetX() + m_pxMysteryShip->GetSprite()->GetRegion()->w / 2, m_pxMysteryShip->GetY());
		}
		if (m_pxMysteryProj->GetY() > m_xSystem.m_iScreenHeight)
		{
			m_pxMysteryProj->Deactivate();
			m_pxMysteryProj->SetPosition(m_pxMysteryShip->GetX() + m_pxMysteryShip->GetSprite()->GetRegion()->w, m_pxMysteryShip->GetY() + m_pxMysteryShip->GetSprite()->GetRegion()->h);
		}
	}*/
	//Alien Fire code
	auto projit = m_apxAlienP.begin();
	auto it = m_apxAlien.begin();
	while (it != m_apxAlien.end())
	{
		(*it)->Update(p_fDeltaTime);
		(*projit)->Update(p_fDeltaTime);
		if ((*projit)->IsVisible() && (*it)->IsVisible())
		{
			
			m_xSystem.m_pxDrawManager->Draw(
				(*projit)->GetSprite(),
				(*projit)->GetX(),
				(*projit)->GetY());
		}
		if ((*projit)->IsActive() == false)
		{
			(*projit)->SetPosition((*it)->GetX() + (*it)->GetSprite()->GetRegion()->w / 2, (*it)->GetY());
		}
		if ((*projit)->GetY() > m_xSystem.m_iScreenHeight)
		{
			(*projit)->Deactivate();
			(*projit)->SetPosition((*it)->GetX() + (*it)->GetSprite()->GetRegion()->w, (*it)->GetY() + (*it)->GetSprite()->GetRegion()->h);
		}
		projit++;
		it++;
	}
	isFalse = false;
	//Code to reverse direction on all Aliens
	if (m_pxAlienRect->GetX() > m_xSystem.m_iScreenWidth - m_pxAlienRect->GetW() && isFalse == false)
	{
		m_pxAlienRect->m_fX = m_xSystem.m_iScreenWidth - m_pxAlienRect->GetW();
		auto it = m_apxAlien.begin();
		while (it != m_apxAlien.end())
		{
			if ((*it)->IsVisible())
			{
				(*it)->Update(p_fDeltaTime);
				(*it)->ReverseDirectionX();
			}
			it++;
		}
		m_pxAlienRect->ReverseDirection();
		isFalse = true;

	}
	isFalse = false;
	if (m_pxAlienRect->GetX() < 0 && isFalse == false)
	{
		m_pxAlienRect->m_fX = 0;
		auto it = m_apxAlien.begin();
		while (it != m_apxAlien.end())
		{
			(*it)->Update(p_fDeltaTime);
			(*it)->ReverseDirectionX();
			it++;
		}
		isFalse = true;
		m_pxAlienRect->ReverseDirection();
	}
	//MysteryShip reversedirection
	if (m_pxMysteryShip->GetX() > m_xSystem.m_iScreenWidth - m_pxMysteryShip->GetSprite()->GetRegion()->w)
	{
		m_pxMysteryShip->m_fX = m_xSystem.m_iScreenWidth - m_pxMysteryShip->GetSprite()->GetRegion()->w;
		m_pxMysteryShip->ReverseDirectionX();
	}
	if (m_pxMysteryShip->GetX() < 0)
	{
		m_pxMysteryShip->m_fX = 0;
		m_pxMysteryShip->ReverseDirectionX();
	}
	//Endstate
	if (m_pxAlienRect->GetY() + m_pxAlienRect->GetH() > m_xSystem.m_iScreenHeight - 100 || life == 3)
	{
		return false;
	}
	if (m_xSystem.m_pxInputManager->IsKeyDown(SDLK_ESCAPE))
	{
		m_bMenu = true;
		return false;
	}
	if (invadersdead > 34)
	{
		m_bWin = true;
		return false;
	}
	CheckCollision();
	return true;
}

void GameState::Draw()
{
	m_xSystem.m_pxDrawManager->Draw(m_pxPlayerShip->GetSprite(), m_pxPlayerShip->GetX(), m_pxPlayerShip->GetY());	//PlayerShip
	m_xSystem.m_pxDrawManager->Draw(m_pxProjectile->GetSprite(), m_pxProjectile->GetX(), m_pxProjectile->GetY());	//Player Projectile
	if (m_pxMysteryShip->IsVisible())
	{
		m_xSystem.m_pxDrawManager->Draw(m_pxMysteryShip->GetSprite(), m_pxMysteryShip->GetX(), m_pxMysteryShip->GetY());	//MysteryShip
	}
	//m_xSystem.m_pxDrawManager->Draw(m_pxAlienRect->GetSprite(), m_pxAlienRect->GetX(), m_pxAlienRect->GetY());	//Alien Rectangle (Debuging)
	auto lifeit = m_apxLife.begin();																				//LifeManager Ships
	while (lifeit != m_apxLife.end())
	{
		if ((*lifeit)->IsVisible())
		{
			m_xSystem.m_pxDrawManager->Draw(
				(*lifeit)->GetSprite(),
				(*lifeit)->GetX(),
				(*lifeit)->GetY());
		}
		lifeit++;
	}
	auto covit = m_apxCover.begin();																				//Covers
	while (covit != m_apxCover.end())
	{
		if ((*covit)->IsVisible())
		{
			m_xSystem.m_pxDrawManager->Draw(
				(*covit)->GetSprite(),
				(*covit)->GetX(),
				(*covit)->GetY());
		}
		covit++;
	}

	auto it = m_apxAlien.begin();																					//Aliens
	while (it != m_apxAlien.end())
	{
		if ((*it)->IsVisible())
		{
			m_xSystem.m_pxDrawManager->Draw(
				(*it)->GetSprite(),
				(*it)->GetX(),
				(*it)->GetY());
		}
		it++;
	}
}

IState* GameState::NextState()
{
	if (!m_bMenu && !m_bWin)
	{
		return new EndState(m_xSystem);
	}
	else if (m_bMenu && !m_bWin)
	{
		return new MenuState(m_xSystem);
	}
	else if (!m_bMenu && m_bWin)
	{
		return new WinState(m_xSystem);
	}
}

void GameState::CheckCollision()
{
	int iOverlapX = 0;
	int iOverlapY = 0;
	
	auto projit = m_apxAlienP.begin();
	while (projit != m_apxAlienP.end() && life < 3)		//Check collision on Alien Projectile and PlayerShip
	{
		if (CollisionManager::Check((*projit)->GetCollider(), m_pxPlayerShip->GetCollider(), iOverlapX, iOverlapY))
		{
			if (abs(iOverlapX) > abs(iOverlapY))
			{
				(*projit)->Deactivate();
				m_apxLife[life]->SetVisible(false);
				life++;
			}
			else
			{
				(*projit)->Deactivate();
				m_apxLife[life]->SetVisible(false);
				life++;
			}
		}
		projit++;
	}

	auto covit = m_apxCover.begin();
	while (covit != m_apxCover.end())					//Check Collision on Cover and Player Projectile
	{
		projit = m_apxAlienP.begin();
		if (CollisionManager::Check((*covit)->GetCollider(), m_pxProjectile->GetCollider(), iOverlapX, iOverlapY))
		{
			if ((*covit)->IsVisible())
			{
				if (abs(iOverlapX) > abs(iOverlapY))
				{
					(*covit)->SetVisible(false);
					m_pxProjectile->Deactivate();
					m_pxProjectile->SetPosition(m_pxPlayerShip->GetX() + m_pxPlayerShip->GetSprite()->GetRegion()->w / 2,
						m_pxPlayerShip->GetY());
				}
				else
				{
					(*covit)->SetVisible(false);
					m_pxProjectile->Deactivate();
					m_pxProjectile->SetPosition(m_pxPlayerShip->GetX() + m_pxPlayerShip->GetSprite()->GetRegion()->w / 2,
						m_pxPlayerShip->GetY());
				}
			}
		}
		while (projit != m_apxAlienP.end())				//Check Collision on Cover and Alien Projectiles
		{
			if (CollisionManager::Check((*covit)->GetCollider(), (*projit)->GetCollider(), iOverlapX, iOverlapY))
			{
				if ((*covit)->IsVisible())
				{
					if (abs(iOverlapX) > abs(iOverlapY))
					{
						(*projit)->Deactivate();
						(*covit)->SetVisible(false);

					}
					else
					{
						(*projit)->Deactivate();
						(*covit)->SetVisible(false);

					}
				}

			}
			projit++;
		}
		covit++;
	}
	auto projit2 = m_apxAlienP.begin();
	auto it = m_apxAlien.begin();
	while (it != m_apxAlien.end())
	{
		if ((*it)->IsVisible())							//Check Collision on Alien and Player Projectile
		{
			if (CollisionManager::Check((*it)->GetCollider(), m_pxProjectile->GetCollider(), iOverlapX, iOverlapY))
			{
				m_pxShoot->PlaySound();

				if (abs(iOverlapX) > abs(iOverlapY))
				{
					m_pxProjectile->Deactivate();
					m_pxProjectile->SetPosition(m_pxPlayerShip->GetX() + m_pxPlayerShip->GetSprite()->GetRegion()->w / 2,
						m_pxPlayerShip->GetY());
					(*it)->SetVisible(false);
					(*projit2)->Deactivate();
					ResizeRect();
					invadersdead++;
					score += 10;
					std::cout << "Score: " << score << std::endl;
				}
				else
				{
					m_pxProjectile->Deactivate();
					m_pxProjectile->SetPosition(m_pxPlayerShip->GetX() + m_pxPlayerShip->GetSprite()->GetRegion()->w / 2,
						m_pxPlayerShip->GetY());
					(*it)->SetVisible(false);
					(*projit2)->Deactivate();
					ResizeRect();
					invadersdead++;
					score += 10;
					std::cout << "Score: " << score << std::endl;
				}
				auto it2 = m_apxAlien.begin();
				while(it2 != m_apxAlien.end())
				{
					(*it2)->SetSpeed(1.05f);
					it2++;
				}
				m_pxAlienRect->SetSpeed(1.05f);
			}
		}
		projit2++;
		it++;
	}
	if (m_pxMysteryShip->IsVisible())					//Check Collision on PlayerProjectile and MysteryShip.
	{
		if (CollisionManager::Check(m_pxProjectile->GetCollider(), m_pxMysteryShip->GetCollider(), iOverlapX, iOverlapY))
		{
			if (abs(iOverlapX) > abs(iOverlapY))
			{
				m_pxProjectile->Deactivate();
				m_pxProjectile->SetPosition(m_pxPlayerShip->GetX() + m_pxPlayerShip->GetSprite()->GetRegion()->w / 2,
					m_pxPlayerShip->GetY());
				m_pxMysteryShip->SetVisible(false);
				m_pxMysteryShip->m_fX = 0;
				m_pxMysteryShip->randomcounter = 0;
				score += 100;
			}
			else
			{
				m_pxProjectile->Deactivate();
				m_pxProjectile->SetPosition(m_pxPlayerShip->GetX() + m_pxPlayerShip->GetSprite()->GetRegion()->w / 2,
					m_pxPlayerShip->GetY());
				m_pxMysteryShip->SetVisible(false);
				m_pxMysteryShip->m_fX = 0;
				m_pxMysteryShip->randomcounter = 0;
				score += 100;
			}
		}
		if (CollisionManager::Check(m_pxMysteryProj->GetCollider(), m_pxPlayerShip->GetCollider(), iOverlapX, iOverlapY))
		{
			if (abs(iOverlapX) > abs(iOverlapY))
			{
				m_pxMysteryProj->Deactivate();
				m_pxMysteryProj->SetPosition(m_pxMysteryShip->GetX() + m_pxMysteryShip->GetSprite()->GetRegion()->w / 2,
					m_pxMysteryShip->GetY());
				m_apxLife[life]->SetVisible(false);
				life++;
			}
			else
			{
				m_pxMysteryProj->Deactivate();
				m_pxMysteryProj->SetPosition(m_pxMysteryShip->GetX() + m_pxMysteryShip->GetSprite()->GetRegion()->w / 2,
					m_pxMysteryShip->GetY());
				m_apxLife[life]->SetVisible(false);
				life++;
			}
		}
	}
}


void GameState::ResizeRect()
{
	int minX = 10000;
	int maxX = -10000;
	int minY = 10000;
	int maxY = -10000;

	auto it = m_apxAlien.begin();
	while (it != m_apxAlien.end())
	{

		if ((*it)->IsVisible())
		{
			Alien* a = (*it);
			if (a->GetX() < minX)
				minX = a->GetX();
			if (a->GetX() + a->GetSprite()->GetRegion()->w > maxX)
				maxX = a->GetX() + a->GetSprite()->GetRegion()->w;
			if (a->GetY() < minY)
				minY = a->GetY();
			if (a->GetY() + a->GetSprite()->GetRegion()->h > maxY)
				maxY = a->GetY() + a->GetSprite()->GetRegion()->h;
		}
		it++;
	}
	
	if (m_pxAlienRect != nullptr)
	{
		float fOldDir = m_pxAlienRect->m_fDirX;
		float fOldSpeed = m_pxAlienRect->m_fSpeed;
		m_xSystem.m_pxSpriteManager->DestroySprite(m_pxAlienRect->GetSprite());
		delete m_pxAlienRect;
		m_pxAlienRect = nullptr;
		m_pxAlienRect = new AlienRect(m_xSystem.m_pxSpriteManager->CreateSprite("../assets/main.bmp",
			0, 40, maxX - minX, maxY - minY), minX, minY, maxX - minX, maxY - minY);
		m_pxAlienRect->m_fDirX = fOldDir;
		m_pxAlienRect->m_fSpeed = fOldSpeed;
	}
	else
	{
		m_pxAlienRect = new AlienRect(m_xSystem.m_pxSpriteManager->CreateSprite("../assets/main.bmp",
			0, 40, maxX - minX, maxY - minY), minX, minY, maxX - minX, maxY - minY);
	}

	
	
}
