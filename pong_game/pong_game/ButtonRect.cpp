#include "stdafx.h"
#include "ButtonRect.h"
#include "MenuState.h"
#include "Keyboard.h"
#include "Engine.h"
#include "StateManager.h"
#include "IState.h"
#include "GameState.h"
#include "InputManager.h"

ButtonRect::ButtonRect(InputManager* p_pxInputManager, Sprite* p_pxSprite, float p_fX, float p_fY, float p_iScreenWidth)
{
	m_fX = p_fX;
	m_fY = p_fY;
	m_pxInputManager = p_pxInputManager;
	m_pxSprite = p_pxSprite;
	m_iScreenWidth = p_iScreenWidth;
	m_bVisible = false;
	m_bRunning = true;
}

ButtonRect::~ButtonRect()
{

}

bool ButtonRect::Update(float p_fDeltaTime)
{
	if (m_pxInputManager->IsKeyDown(SDLK_w))
	{
		m_bVisible = false;
		return false;
	}
	if (m_pxInputManager->IsKeyDown(SDLK_s))
	{
		m_bVisible = true;
		return false;
	}

	if (m_pxInputManager->IsKeyDown(SDLK_RETURN) && m_bVisible == false)
	{
		return true;
	}
	else if (m_pxInputManager->IsKeyDown(SDLK_RETURN) && m_bVisible == true)
	{
		SDL_Quit();
		return false;
	}
}

float ButtonRect::GetX()
{
	return m_fX;
}

float ButtonRect::GetY()
{
	return m_fY;
}

bool ButtonRect::IsVisible()
{
	return m_bVisible;
}

bool ButtonRect::IsRunning()
{
	return m_bRunning;
}

Sprite* ButtonRect::GetSprite()
{
	return m_pxSprite;
}