#pragma once

class InputManager
{
public:
	InputManager();
	~InputManager();
	void Initialize();
	void Shutdown();
	void Update();
	int GetMouseX();
	int GetMouseY();
	void SetMousePosition(int p_iX, int p_iY);
	bool IsMouseButtonDown(int p_iIndex);
	void SetMouseButton(int p_iIndex, bool p_bValue);
	bool IsKeyDown(int p_iIndex);
	void SetKey(int p_iIndex, bool p_bValue);
private:
	SDL_Event xEvent;

	bool m_abButtons[3];
	bool m_abKeys[256];
	int m_iX;
	int m_iY;
};