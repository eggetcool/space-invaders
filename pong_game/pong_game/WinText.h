#pragma once
#include "IEntity.h"

class WinText : public IEntity
{
public:
	WinText(Sprite* p_pxSprite, float p_fX, float p_fY);
	~WinText();
	void Update(float p_fDeltaTime);
	float GetX();
	float GetY();
	Sprite* GetSprite();
	Collider* GetCollider();

private:

	float m_fX;
	float m_fY;
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	bool IsVisible() { return true; };
	EENTITYTYPE GetType() { return EENTITYTYPE::ENTITY_TEXT; };
};
