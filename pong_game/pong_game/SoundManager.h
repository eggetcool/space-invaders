#pragma once

class Sound;

class SoundManager
{
public:
	SoundManager();
	~SoundManager();
	bool Initialize();
	void Shutdown();
	Sound* CreateSound(const std::string& p_sFilepath);
	void DestroySound(Sound* p_pxSound);
private:
	std::vector<Sound*> m_apxSounds;
	std::map<std::string, Mix_Chunk*> m_apxAudio;
};