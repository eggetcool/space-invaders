#include "stdafx.h"
#include "MysteryShip.h"
#include "Sprite.h"
#include "Collider.h"
#include "MysteryShipProjectile.h"
#include <cstdlib>
#include <ctime>

MysteryShip::MysteryShip(Sprite* p_pxSprite, float p_fX, float p_fY)
{
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_fDirX = 1.0f;
	m_fSpeed = 200;
	srand(time(0));
	randomcounter = 0;
	m_pxCollider = new Collider(
		p_pxSprite->GetRegion()->w,
		p_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->Refresh();
}

MysteryShip::~MysteryShip()
{
	delete m_pxCollider;
	m_pxCollider = nullptr;
}

void MysteryShip::Update(float p_fDeltaTime)
{
	random = 1 + (rand() % 300);
	if (random == 1)
	{
		m_bVisible = true;
		randomcounter++;
	}
	if (m_bVisible == true)
	{
		m_fX += m_fDirX * p_fDeltaTime * m_fSpeed;
		if (random == 1)
		{
			m_bVisible = true;
			randomcounter++;
		}
		m_pxCollider->Refresh();

	}
	if (randomcounter > 10)
	{
		m_fX = 0;
		randomcounter = 0;
		m_bVisible = false;
		m_pxCollider->Refresh();
	}
	
	
}

Sprite* MysteryShip::GetSprite()
{
	return m_pxSprite;
}

Collider* MysteryShip::GetCollider()
{
	return m_pxCollider;
}

float MysteryShip::GetX()
{
	return m_fX;
}

float MysteryShip::GetY()
{
	return m_fY;
}

bool MysteryShip::IsVisible()
{
	return m_bVisible;
}

EENTITYTYPE MysteryShip::GetType()
{
	return EENTITYTYPE::ENTITY_ALIEN;
}

void MysteryShip::ReverseDirectionX()
{
	m_fDirX *= -1;
}

void MysteryShip::SetVisible(bool p_bValue)
{
	m_bVisible = p_bValue;
}

void MysteryShip::SetSpeed(float p_fSpeed)
{
	m_fSpeed *= p_fSpeed;
}
