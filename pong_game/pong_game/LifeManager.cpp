#include "stdafx.h"
#include "LifeManager.h"
#include "Sprite.h"

LifeManager::LifeManager(Sprite* p_pxSprite, float p_fX, float p_fY)
{
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_bVisible = true;
}

LifeManager::~LifeManager()
{
}

void LifeManager::Update(float p_fDeltaTime)
{
}

Sprite* LifeManager::GetSprite()
{
	return m_pxSprite;
}

Collider * LifeManager::GetCollider()
{
	return m_pxCollider;
}

float LifeManager::GetX()
{
	return m_fX;
}

float LifeManager::GetY()
{
	return m_fY;
}

bool LifeManager::IsVisible()
{
	return m_bVisible;
}

void LifeManager::SetVisible(bool p_bValue)
{
	m_bVisible = p_bValue;
}

EENTITYTYPE LifeManager::GetType()
{
	return EENTITYTYPE::ENTITY_PLAYERLIFE;
}
