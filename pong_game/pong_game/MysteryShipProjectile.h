#pragma once
#include "IEntity.h"


class MysteryShipProjectile : public IEntity
{
public:
	MysteryShipProjectile(Sprite* p_pxSprite,
		int p_iScreenWidth, int p_iScreenHeight);
	~MysteryShipProjectile();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	bool IsActive();

	EENTITYTYPE GetType();

	void Activate();
	void Deactivate();
	void SetPosition(float p_fX, float p_fY);
private:
	MysteryShipProjectile() {};
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	float m_fSpeed;
	float m_fDirY;
	float m_fX;
	float m_fY;
	int m_iScreenWidth;
	int m_iScreenHeight;
	bool m_bVisible;
	bool m_bActive;
	int random;
};