#pragma once

#include "IEntity.h"
class Keyboard;
class InputManager;

class PlayerShip : public IEntity
{
public:
	PlayerShip(InputManager* p_pxInputManager, Sprite* p_pxSprite,
		float p_fX, float p_fY, int p_iScreenWidth);
	~PlayerShip();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	void Life(int);
	bool IsVisible();
	EENTITYTYPE GetType();
private:
	PlayerShip() {};
	Keyboard* m_pxKeyboard;
	InputManager* m_pxInputManager;
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	
	float m_fX = 0;
	float m_fY;
	int m_iScreenWidth;
	bool m_bVisible;
	int life;
};