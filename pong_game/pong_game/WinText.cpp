#include "stdafx.h"
#include "WinText.h"
#include "Collider.h"

WinText::WinText(Sprite* p_pxSprite, float p_fX, float p_fY)
{
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
}

WinText::~WinText()
{
}

void WinText::Update(float p_fDeltaTime)
{
}

float WinText::GetX()
{
	return m_fX;
}

float WinText::GetY()
{
	return m_fY;
}

Sprite* WinText::GetSprite()
{
	return m_pxSprite;
}

Collider* WinText::GetCollider()
{
	return m_pxCollider;
}
