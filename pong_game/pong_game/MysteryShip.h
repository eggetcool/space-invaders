#pragma once
#include "IEntity.h"
class MysteryShipProjectile;

class MysteryShip : public IEntity
{
public:
	MysteryShip(Sprite* p_pxSprite, float p_fX, float p_fY);
	~MysteryShip();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();
	void ReverseDirectionX();
	void SetVisible(bool p_bValue);
	void SetSpeed(float p_fSpeed);
	float m_fX;
	float m_fDirX;
	float m_fSpeed;
	int randomcounter;
private:
	Sprite* m_pxSprite;
	Collider* m_pxCollider;
	MysteryShipProjectile* m_pxAlienProj;
	float m_fY;
	bool m_bVisible = false;
	int random;
	
	bool MysteryprojActive = false;
};