#pragma once
#include "IState.h"

class ButtonRect;
//class Keyboard;
class InputManager;


class MenuState : public IState
{
public:
	MenuState(System& p_xSystem);
	~MenuState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
	void Draw();
	void ChangeSprite();
	IState* NextState();
private:
	System m_xSystem;
	ButtonRect* m_pxButtonRect;
	ButtonRect* m_pxButtonRect2;
	ButtonRect* m_pxButtonRect3;
	ButtonRect* m_pxButtonRect4;
	//Keyboard* m_pxKeyboard;
	InputManager* m_pxInputManager;
	bool m_bVisible;
	bool m_bRunning;
};