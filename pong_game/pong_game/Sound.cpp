#include "stdafx.h"
#include "Sound.h"

Sound::Sound(Mix_Chunk* p_pxsoundClip)
{
	m_pxSoundClip = p_pxsoundClip;
}

Sound::~Sound()
{

}

void Sound::PlaySound()
{
	Mix_PlayChannel(-1, m_pxSoundClip, 0);
}
