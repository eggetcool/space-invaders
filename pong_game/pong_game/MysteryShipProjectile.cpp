#include "stdafx.h"
#include "MysteryShipProjectile.h"
#include "Sprite.h"
#include "Collider.h"
#include "MysteryShip.h"
#include <cstdlib>
#include <ctime>

MysteryShipProjectile::MysteryShipProjectile(Sprite * p_pxSprite, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxSprite = p_pxSprite;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_fSpeed = 0.0f;
	m_fDirY = 0.0f;
	m_fX = 0.0f;
	m_fY = 0.0f;
	srand(time(0));
	m_bVisible = true;
	m_bActive = false;
	m_pxCollider = new Collider(
		m_pxSprite->GetRegion()->w,
		m_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
}

MysteryShipProjectile::~MysteryShipProjectile()
{
	delete m_pxCollider;
	m_pxCollider = nullptr;
}

void MysteryShipProjectile::Update(float p_fDeltaTime)
{
	random = 1 + (rand() % 100);
	if (m_bActive)
	{
		//Funkar inte
		m_fY += m_fDirY * m_fSpeed * p_fDeltaTime;
		m_pxCollider->Refresh();
	}
	else if (random == 1)
	{
		Activate();
	}
}

Sprite* MysteryShipProjectile::GetSprite()
{
	return m_pxSprite;
}

Collider* MysteryShipProjectile::GetCollider()
{
	return m_pxCollider;
}

float MysteryShipProjectile::GetX()
{
	return m_fX;
}

float MysteryShipProjectile::GetY()
{
	return m_fY;
}

bool MysteryShipProjectile::IsVisible()
{
	return m_bVisible;
}

bool MysteryShipProjectile::IsActive()
{
	return m_bActive;
}

EENTITYTYPE MysteryShipProjectile::GetType()
{
	return EENTITYTYPE::ENTITY_PROJECTILE;
}

void MysteryShipProjectile::Activate()
{
	m_bActive = true;
	m_fSpeed = 200;
	m_fDirY = 2.0f;
	std::cout << " KLAASDAA" << std::endl;
}

void MysteryShipProjectile::Deactivate()
{
	m_bActive = false;
	m_fSpeed = 0;
	m_fDirY = 0.0f;
}

void MysteryShipProjectile::SetPosition(float p_fX, float p_fY)
{
	m_fX = p_fX;
	m_fY = p_fY;
	m_pxCollider->Refresh();
}
