#pragma once
#include "IState.h"


class PlayerShip;
class Projectile;
class Alien;
class AlienProj;
class AlienRect;
class StateManager;
class EndState;
class WinState;
class Cover;
class LoseText;
class LifeManager;
class MysteryShip;
class MysteryShipProjectile;
class Sound;

class GameState : public IState
{
public:
	GameState(System& p_xSystem);
	~GameState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
	void Draw();
	IState* NextState();
private:
	void CheckCollision();
	void ResizeRect();
	int life;
	int invadersdead;
	int score;
	bool isFalse = false;
	bool m_bLose = false;
	bool m_bMenu = false;
	bool m_bWin = false;
	System m_xSystem;
	EndState* m_pxEndState;
	WinState* m_pxWinState;
	StateManager* m_pxStateManager;
	PlayerShip* m_pxPlayerShip;
	Projectile* m_pxProjectile;
	AlienRect* m_pxAlienRect;
	AlienProj* m_pxAlienProj;
	MysteryShipProjectile* m_pxMysteryProj;
	Cover* m_pxCover;
	//TTF_Font* gFont;
	SDL_Surface* message = NULL;
	SDL_Color textColor;
	LoseText* m_pxLoseText;
	LifeManager* m_pxLifeManager;
	MysteryShip* m_pxMysteryShip;
	Sound* m_pxShoot;
	Mix_Music* m_pxMusic;

	std::vector<AlienProj*> m_apxAlienP;
	std::vector<Alien*> m_apxAlien;
	std::vector<Cover*> m_apxCover;
	std::vector<LifeManager*> m_apxLife;
};