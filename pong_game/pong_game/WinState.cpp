#include "stdafx.h"
#include "WinState.h"
#include "WinText.h"
#include "ButtonRect.h"
#include "Keyboard.h"
#include "StateManager.h"
#include "DrawManager.h"
#include "Engine.h"
#include "IState.h"
#include "SpriteManager.h"
#include "Sprite.h"
#include "GameState.h"

WinState::WinState(System& p_pxSystem)
{
	m_xSystem = p_pxSystem;
	m_pxWintext = nullptr;
	m_pxKeyboard = nullptr;
	m_pxButtonRect = nullptr;
	m_pxButtonRect2 = nullptr;
	m_pxButtonRect3 = nullptr;
	m_pxButtonRect4 = nullptr;
}

WinState::~WinState()
{
}

void WinState::Enter()
{
	int buttonWidth = 250;
	int buttonHeight = 200;

	Sprite* xSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/main.bmp", 92, 21, 76, 21);

	Sprite* xButton = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/button.bmp", 0, 0, 100, 30);
	Sprite* xButtonmark = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/buttonmarked.bmp", 0, 0, 100, 30);

	Sprite* xButton2 = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/button2.bmp", 0, 0, 100, 30);
	Sprite* xButton2mark = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/button2marked.bmp", 0, 0, 100, 30);

	SDL_Rect* xButtonRect = xButton->GetRegion();

	m_pxWintext = new WinText(xSprite, m_xSystem.m_iScreenWidth / 2 - 36,
		(m_xSystem.m_iScreenHeight / 2) - 100);

	m_pxButtonRect = new ButtonRect(m_xSystem.m_pxInputManager, xButton, buttonWidth - 50, buttonHeight, m_xSystem.m_iScreenWidth);
	m_pxButtonRect2 = new ButtonRect(m_xSystem.m_pxInputManager, xButtonmark, buttonWidth - 50, buttonHeight, m_xSystem.m_iScreenWidth);
	m_pxButtonRect3 = new ButtonRect(m_xSystem.m_pxInputManager, xButton2mark, buttonWidth - 50, buttonHeight + 50, m_xSystem.m_iScreenWidth);
	m_pxButtonRect4 = new ButtonRect(m_xSystem.m_pxInputManager, xButton2, buttonWidth - 50, buttonHeight + 50, m_xSystem.m_iScreenWidth);
}

void WinState::Exit()
{
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxButtonRect4->GetSprite());
	delete m_pxButtonRect4;
	m_pxButtonRect4 = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxButtonRect3->GetSprite());
	delete m_pxButtonRect3;
	m_pxButtonRect3 = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxButtonRect2->GetSprite());
	delete m_pxButtonRect2;
	m_pxButtonRect2 = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxButtonRect->GetSprite());
	delete m_pxButtonRect;
	m_pxButtonRect = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxWintext->GetSprite());
	delete m_pxWintext;
	m_pxWintext = nullptr;
}

bool WinState::Update(float p_fDeltaTime)
{
	if (m_pxButtonRect->Update(p_fDeltaTime))
	{
		return false;
	}
	return true;
}

void WinState::Draw()
{
	m_xSystem.m_pxDrawManager->Draw(m_pxWintext->GetSprite(), m_pxWintext->GetX(), m_pxWintext->GetY());
	if (m_pxButtonRect->IsVisible())
	{
		m_xSystem.m_pxDrawManager->Draw(m_pxButtonRect3->GetSprite(), m_pxButtonRect3->GetX(), m_pxButtonRect3->GetY());
		m_xSystem.m_pxDrawManager->Draw(m_pxButtonRect->GetSprite(), m_pxButtonRect->GetX(), m_pxButtonRect->GetY());
	}
	else
	{
		ChangeSprite();
	}
}

void WinState::ChangeSprite()
{
	m_xSystem.m_pxDrawManager->Draw(m_pxButtonRect4->GetSprite(), m_pxButtonRect4->GetX(), m_pxButtonRect4->GetY());

	m_xSystem.m_pxDrawManager->Draw(m_pxButtonRect2->GetSprite(), m_pxButtonRect2->GetX(), m_pxButtonRect2->GetY());
}

IState* WinState::NextState()
{
	return new GameState(m_xSystem);
}
