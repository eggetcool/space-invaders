#pragma once


enum MENUENTITYTYPE
{
	ENTITY_BUTTON
};

class Sprite;
//class Keyboard;
class InputManager;
class Engine;
class StateManager;
class MenuState;


class ButtonRect
{
public:
	ButtonRect(InputManager* m_pxInputManager, Sprite* p_pxSprite, float p_fX, float p_fY, float p_iScreenWidth);
	~ButtonRect();
	bool Update(float p_fDeltaTime);
	float GetX();
	float GetY();
	bool IsVisible();
	bool IsRunning();

	Sprite* GetSprite();


private:
	MENUENTITYTYPE GetType() { return MENUENTITYTYPE::ENTITY_BUTTON; };
	int m_fX;
	int m_fY;
	int m_iScreenWidth;
	bool m_bVisible;
	bool m_bRunning;
	Sprite* m_pxSprite;
	InputManager* m_pxInputManager;
	StateManager* m_pxStateManager;
	Engine* m_pxEngine;
	MenuState* m_pxMenuState;

}; 