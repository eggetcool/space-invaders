#pragma once
#include "IEntity.h"

class LifeManager : public IEntity
{
public:
	LifeManager(Sprite* p_pxSprite, float p_fX, float p_fY);
	~LifeManager();
	void Update(float p_fDeltaTime);
	Sprite* GetSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	bool IsVisible();
	void SetVisible(bool p_bValue);
	EENTITYTYPE GetType();
private:
	LifeManager() {};
	Sprite* m_pxSprite;
	Collider* m_pxCollider;

	float m_fX;
	float m_fY;
	bool m_bVisible;
};