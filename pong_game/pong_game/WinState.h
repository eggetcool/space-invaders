#pragma once
#include "IState.h"

class ButtonRect;
class WinText;
class Keyboard;

class WinState : public IState
{
public:
	WinState(System& p_pxSystem);
	~WinState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
	void Draw();
	void ChangeSprite();
	IState* NextState();
private:
	bool m_bRunning;
	bool m_bVisible;
	System m_xSystem;
	Keyboard* m_pxKeyboard;
	WinText* m_pxWintext;
	ButtonRect* m_pxButtonRect;
	ButtonRect* m_pxButtonRect2;
	ButtonRect* m_pxButtonRect3;
	ButtonRect* m_pxButtonRect4;
};